package org.retiznyk.animals.core.contant;

/**
 * The class contains common constants which are used in the app.
 *
 * @author Arsen_Retiznyk
 **/
public final class AppConstants {

    private AppConstants() {
    }

    /**
     * The class contains names of categories.
     *
     * @author Arsen_Retiznyk
     */
    public static final class CategoriesNames {

        private CategoriesNames() {
        }

        public static final String ANIMALS = "ANIMALS";
        public static final String NUMBERS = "NUMBERS";
        public static final String CARS = "CARS";
    }
}
