package org.retiznyk.animals.core.categories.matcher;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;

/**
 * Matcher by name of categories.
 *
 * @author Arsen_Retiznyk
 */
public class CategoryNameMatcher implements CategoryMatcher {

    private final String categoryName;

    /**
     * Create a new matcher.
     *
     * @param name - name of a category that will be used to identify the category.
     */
    public CategoryNameMatcher(@NotNull final String name) {
        this.categoryName = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCategory(String line) {
        return categoryName.equals(line);
    }
}
