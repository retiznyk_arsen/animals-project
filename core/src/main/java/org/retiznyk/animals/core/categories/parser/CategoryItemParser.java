package org.retiznyk.animals.core.categories.parser;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.core.categories.descriptor.CategoryDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * The parser determines to which category the item should be added.
 *
 * The parser uses finite-state machine model to choose a category for an items.
 * The instances of the class are not thread safe.
 * @author Arsen_Retiznyk
 */
@Component
@Scope("prototype")
public class CategoryItemParser {

    private CategoryDescriptor current;

    private List<CategoryDescriptor> categoryDescriptors;

    /**
     * @param categoryDescriptors - a list of exists descriptors for categories.
     */
    public CategoryItemParser(@Autowired final List<CategoryDescriptor> categoryDescriptors) {
        current = new CategoryDescriptor(item -> true, StringUtils.EMPTY);

        this.categoryDescriptors = Collections.unmodifiableList(categoryDescriptors);
    }

    /**
     * Returns a category for an item.
     *
     * @param line - data for processing
     * @return entry that contains category descriptor as a key and an item as a value
     */
    public Map.Entry<CategoryDescriptor, String> getSubcategoryDefinition(@NotNull final String line) {
        return new AbstractMap.SimpleEntry<>(current, line);
    }

    /**
     * Checks that the passed parameter is not category name.
     *
     * @param line - data for processing
     * @return true if passed parameter is not category name or false if is
     */
    public boolean isCategoryItem(final String line) {
        changeStateIfIsNewCategory(line);
        return isNotCategoryName(line);
    }

    /**
     * Returns list of all category descriptors which were used to identify items of the categories.
     *
     * @return list of {@link CategoryDescriptor}
     */
    public List<CategoryDescriptor> getCategoryDescriptors() {
        return categoryDescriptors;
    }

    private boolean isNotCategoryName(String line) {
        return !current.getCategoryMatcher().isCategory(line);
    }

    private void changeStateIfIsNewCategory(String line) {
        current = categoryDescriptors.stream()
                .filter(cm -> cm.getCategoryMatcher().isCategory(line)).findFirst().orElse(current);
    }
}
