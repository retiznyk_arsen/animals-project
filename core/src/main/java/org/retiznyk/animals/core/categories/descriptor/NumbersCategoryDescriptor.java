package org.retiznyk.animals.core.categories.descriptor;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;
import org.retiznyk.animals.api.categories.model.CategoryModel;
import org.retiznyk.animals.core.categories.model.NumbersCategoryModel;

import java.util.Optional;

/**
 * {@inheritDoc}.
 *
 * Additionally counts the number of identical items in the category.
 *
 * @author Arsen_Retiznyk
 **/
public class NumbersCategoryDescriptor extends CategoryDescriptor {

    private static final int ZERO_COUNT = 0;
    private final NumbersCategoryModel categoryModel;
    private final CategoryMatcher categoryMatcher;

    /**
     * Create new category descriptor.
     *
     * @param matcher - matcher to identify a category
     * @param categoryName - name of the category
     */
    public NumbersCategoryDescriptor(@NotNull final CategoryMatcher matcher,
                              @NotNull final String categoryName) {
        super(matcher, categoryName);
        this.categoryMatcher = matcher;
        this.categoryModel = new NumbersCategoryModel(categoryName);
    }

    @Override
    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    @Override
    public CategoryMatcher getCategoryMatcher() {
        return categoryMatcher;
    }

    /**
     * The method allows adding new items to category.
     *
     * @param item - a item that will be added to category
     */
    @Override
    public void addItem(@NotNull final String item) {
        categoryModel.getMapItems().compute(item, (key, count) -> Optional.ofNullable(count).orElse(ZERO_COUNT) + 1);
    }
}
