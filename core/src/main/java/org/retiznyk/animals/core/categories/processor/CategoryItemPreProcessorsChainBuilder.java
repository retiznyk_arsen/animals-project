package org.retiznyk.animals.core.categories.processor;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * The processor can be used to build a chain with other processors.
 *
 * Added processors will be performed in adding order.
 *
 * @author Arsen_Retiznyk
 */
public final class CategoryItemPreProcessorsChainBuilder {
    private List<CategoryItemPreProcessor> processorsChain;

    private CategoryItemPreProcessorsChainBuilder() {
        this.processorsChain = new ArrayList<>();
    }

    /**
     * Create a new builder.
     *
     * @return - created builder.
     */
    public static CategoryItemPreProcessorsChainBuilder createChainBuilder() {
        return new CategoryItemPreProcessorsChainBuilder();
    }

    /**
     * Adds a new processor to chain.
     *
     * @param postProcessor - instance of processor
     * @return current instance of the builder
     */
    public CategoryItemPreProcessorsChainBuilder add(@NotNull final CategoryItemPreProcessor postProcessor) {
        processorsChain.add(postProcessor);
        return this;
    }

    /**
     * Builds chain of processors.
     *
     * Added processors will be performed in adding order.
     * @return a new processor that includes chain of added processors
     */
    public CategoryItemPreProcessor build() {
        return (line) -> processorsChain.stream().reduce(line,
                (currentLine, processor) -> processor.apply(currentLine),
                (processor, nextProcessor) -> nextProcessor);
    }
}
