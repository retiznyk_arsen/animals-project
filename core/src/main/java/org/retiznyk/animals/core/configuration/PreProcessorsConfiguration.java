package org.retiznyk.animals.core.configuration;

import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;
import org.retiznyk.animals.core.categories.processor.CategoryItemPreProcessorsChainBuilder;
import org.retiznyk.animals.core.categories.processor.HashAppenderCategoryItemPreProcessor;
import org.retiznyk.animals.core.categories.processor.ToLowerCaseCategoryItemPreProcessor;
import org.retiznyk.animals.core.configuration.annotation.Category;
import org.retiznyk.animals.core.contant.AppConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The configuration contains preprocessors for category items.
 *
 * @author Arsen_Retiznyk
 **/
@Configuration
public class PreProcessorsConfiguration {

    /**
     * The bean of a preprocessor that includes {@link ToLowerCaseCategoryItemPreProcessor}
     * and {@link HashAppenderCategoryItemPreProcessor} preprocessors.
     */
    @Bean
    @Category(AppConstants.CategoriesNames.CARS)
    public CategoryItemPreProcessor carsPostProcessor() {
        return CategoryItemPreProcessorsChainBuilder.createChainBuilder()
                .add(new ToLowerCaseCategoryItemPreProcessor())
                .add(new HashAppenderCategoryItemPreProcessor())
                .build();
    }
}
