package org.retiznyk.animals.core.categories.descriptor;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;
import org.retiznyk.animals.api.categories.model.CategoryModel;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * The class contains additional information about a category.
 *
 * The class allows binding the category definition
 * and its behavior or other additional data.
 *
 * @author Arsen_Retiznyk
 */
public class CarsCategoryDescriptor extends CategoryDescriptor {
    private final CategoryItemPreProcessor categoryItemsPostProcessor;

    /**
     * Create new category descriptor.
     *
     * @param matcher - matcher to identify a category
     * @param categoryName - name of the category
     * @param categoryItemsPreProcessor - a processor to pre-process an item of the category before adding to the category
     * @param categoryItemsComparator - a comparator to choose order of items of the category
     */
    public CarsCategoryDescriptor(@NotNull final CategoryMatcher matcher,
                                  @NotNull final String categoryName,
                                  @NotNull final CategoryItemPreProcessor categoryItemsPreProcessor,
                                  @NotNull final Comparator<String> categoryItemsComparator) {
        super(matcher, new CategoryModel(categoryName, new TreeSet<>(categoryItemsComparator)));
        this.categoryItemsPostProcessor = categoryItemsPreProcessor;
    }

    /**
     * The method processes item {@link CategoryItemPreProcessor} before adding the item to a category.
     * @param item - a item that will be added to category
     */
    @Override
    public void addItem(@NotNull String item) {
        super.addItem(categoryItemsPostProcessor.apply(item));
    }
}
