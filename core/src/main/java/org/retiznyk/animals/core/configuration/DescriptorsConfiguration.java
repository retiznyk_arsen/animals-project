package org.retiznyk.animals.core.configuration;

import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;
import org.retiznyk.animals.core.categories.descriptor.CarsCategoryDescriptor;
import org.retiznyk.animals.core.categories.descriptor.CategoryDescriptor;
import org.retiznyk.animals.core.categories.descriptor.NumbersCategoryDescriptor;
import org.retiznyk.animals.core.categories.matcher.CategoryNameMatcher;
import org.retiznyk.animals.core.configuration.annotation.Category;
import org.retiznyk.animals.core.contant.AppConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Comparator;

/**
 * The configuration contains descriptors for different categories.
 *
 * @author Arsen_Retiznyk
 **/
@Configuration
public class DescriptorsConfiguration {

    /**
     * Animals category descriptor.
     */
    @Bean
    @Scope("prototype")
    @Category(AppConstants.CategoriesNames.ANIMALS)
    public CategoryDescriptor animalsDescriptor() {
        return new CategoryDescriptor(new CategoryNameMatcher(AppConstants.CategoriesNames.ANIMALS),
                AppConstants.CategoriesNames.ANIMALS);
    }

    /**
     * Numbers category descriptor.
     */
    @Bean
    @Scope("prototype")
    @Category(AppConstants.CategoriesNames.NUMBERS)
    public CategoryDescriptor numbersDescriptor() {
        return new NumbersCategoryDescriptor(new CategoryNameMatcher(AppConstants.CategoriesNames.NUMBERS),
                AppConstants.CategoriesNames.NUMBERS);
    }

    /**
     * Cars category descriptor.
     *
     * @param categoryItemPreProcessor - the processor that will be used to perform additional operations for category items
     */
    @Bean
    @Scope("prototype")
    @Category(AppConstants.CategoriesNames.CARS)
    public CategoryDescriptor carsDescriptor(@Category(AppConstants.CategoriesNames.CARS) final CategoryItemPreProcessor categoryItemPreProcessor) {
        return new CarsCategoryDescriptor(new CategoryNameMatcher(AppConstants.CategoriesNames.CARS),
                AppConstants.CategoriesNames.CARS, categoryItemPreProcessor, Comparator.reverseOrder());
    }


}
