package org.retiznyk.animals.core.categories.processor;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;

/**
 * Transforms an item name to lowercase.
 *
 * @author Arsen_Retiznyk
 */
public class ToLowerCaseCategoryItemPreProcessor implements CategoryItemPreProcessor {

    /**
     * Transforms an item name to lowercase.
     *
     * @param categoryItem - a name of the item
     * @return a new string that contains the item name in lowercase
     */
    @Override
    public String apply(@NotNull String categoryItem) {
        return categoryItem.toLowerCase();
    }
}
