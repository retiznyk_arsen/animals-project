package org.retiznyk.animals.core;

import org.retiznyk.animals.api.categories.model.CategoryModel;
import org.retiznyk.animals.core.categories.parser.CategoryItemParser;
import org.retiznyk.animals.core.categories.descriptor.CategoryDescriptor;
import org.retiznyk.animals.core.utils.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Main class to run the application.
 *
 * @author Arsen_Retiznyk
 */
public class Main {

    private static final String DEFAULT_INPUT_DATA = "input2.txt";

    public static void main(String[] args) throws IOException, URISyntaxException {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        CategoryItemParser cc = ctx.getBean(CategoryItemParser.class);

        String resourceName = Arrays.stream(args).findAny().orElse(DEFAULT_INPUT_DATA);

        FileUtils.readFileFromClasspath(resourceName)
                .filter(cc::isCategoryItem)
                .map(cc::getSubcategoryDefinition)
                .forEach(i -> i.getKey().addItem(i.getValue()));

        String result = cc.getCategoryDescriptors().stream()
                .map(CategoryDescriptor::getCategoryModel)
                .map(CategoryModel::toString)
                .collect(Collectors.joining(System.lineSeparator() + System.lineSeparator()));

        System.out.println(result);
    }
}
