package org.retiznyk.animals.core.utils;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Util class that contains methods to work with files.
 *
 * @author Arsen_Retiznyk
 */
public final class FileUtils {

    private FileUtils(){
    }

    private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);

    private static final String JAR_SCHEME = "jar";

    /**
     * Allows read a resource as {@link Stream} from the classpath.
     *
     * @param path - path to the resource
     * @return instance of {@link Stream} where each item represent new line
     * @throws URISyntaxException - when a passed {@param path} cannot be transformed to {@link URI}
     * @throws IOException - when the resource cannot be read from the classpath
     * @throws IllegalArgumentException - when
     */
    public static Stream<String> readFileFromClasspath(@NotNull final String path) throws URISyntaxException, IOException {
        URI uri = Optional.ofNullable(ClassLoader.getSystemResource(path))
                .orElseThrow(() -> new IllegalArgumentException("Cannot find resource " + path))
                .toURI();
        Optional.of(JAR_SCHEME.equals(uri.getScheme())).filter(result -> result).ifPresent(res -> {
            try {
                FileSystems.newFileSystem(uri, Collections.emptyMap());
            } catch (IOException e) {
                LOG.error("Cannot read data from jar.", e);
            }
        });

        return Files.lines(Paths.get(uri), StandardCharsets.UTF_8);
    }
}
