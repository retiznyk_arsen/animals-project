package org.retiznyk.animals.core.categories.processor;

import org.apache.commons.codec.digest.DigestUtils;
import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;

/**
 * Adds MD5 hash to a name of an item of a category.
 *
 * @author Arsen_Retiznyk
 */
public class HashAppenderCategoryItemPreProcessor implements CategoryItemPreProcessor {

    /**
     * Appends MD5 hash to a name of an item of a category.
     *
     * @param categoryItem - a name of the item
     * @return a new string that includes MD5 hash.
     */
    @Override
    public String apply(String categoryItem) {
        return categoryItem + " (" + DigestUtils.md5Hex(categoryItem) + ")";
    }
}
