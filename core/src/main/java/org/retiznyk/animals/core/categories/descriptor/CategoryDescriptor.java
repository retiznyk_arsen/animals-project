package org.retiznyk.animals.core.categories.descriptor;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;
import org.retiznyk.animals.api.categories.model.CategoryModel;

import java.util.TreeSet;

/**
 * The class contains additional information about a category.
 *
 * The class allows binding the category definition
 * and its behavior or other additional data.
 *
 * @author Arsen_Retiznyk
 */
public class CategoryDescriptor {

    private final CategoryModel categoryModel;
    private final CategoryMatcher categoryMatcher;

    /**
     * Create new category descriptor.
     *
     * @param matcher - matcher to identify a category
     * @param categoryName - name of the category
     */
    public CategoryDescriptor(@NotNull final CategoryMatcher matcher,
                              @NotNull final String categoryName) {
        this.categoryMatcher = matcher;
        this.categoryModel = new CategoryModel(categoryName, new TreeSet<>());
    }

    /**
     * Create new category descriptor.
     *
     * @param matcher - matcher to identify a category
     * @param categoryModel - the model that represent the category
     */
    protected CategoryDescriptor(@NotNull final CategoryMatcher matcher,
                                 @NotNull final CategoryModel categoryModel) {
        this.categoryMatcher = matcher;
        this.categoryModel = categoryModel;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public CategoryMatcher getCategoryMatcher() {
        return categoryMatcher;
    }

    /**
     * The method allows adding new items to category.
     *
     * @param item - a item that will be added to category
     */
    public void addItem(@NotNull final String item) {
        categoryModel.getItems().add(item);
    }
}
