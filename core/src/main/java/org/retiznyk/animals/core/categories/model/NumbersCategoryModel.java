package org.retiznyk.animals.core.categories.model;

import org.jetbrains.annotations.NotNull;
import org.retiznyk.animals.api.categories.model.CategoryModel;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * The model additionally contains a count of the identical items.
 *
 * @author Arsen_Retiznyk
 **/
public class NumbersCategoryModel extends CategoryModel {

    private final Map<String, Integer> mapItems;

    /**
     * New numbers category model.
     *
     * @param name - the name of category
     */
    public NumbersCategoryModel(@NotNull String name) {
        super(name);
        this.mapItems = new TreeMap<>();
    }

    public Map<String, Integer> getMapItems() {
        return mapItems;
    }

    @Override
    public Collection<String> getItems() {
        return mapItems.keySet();
    }

    @Override
    public String toString() {
        return getName() + System.lineSeparator() + mapItems.entrySet().stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue())
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
