package org.retiznyk.animals.core.categories.processor;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test {@link ToLowerCaseCategoryItemPreProcessor}.
 *
 * @author Arsen_Retiznyk
 **/
public class ToLowerCaseCategoryItemPreProcessorTest {

    private static final String TEST_VALUE = "TestValue";
    private static final String EXPECTED_VALUE = "testvalue";

    private ToLowerCaseCategoryItemPreProcessor target = new ToLowerCaseCategoryItemPreProcessor();

    @Test
    public void shouldTransformPassedParameterToLowercase() {
        Assert.assertEquals(EXPECTED_VALUE, target.apply(TEST_VALUE));
    }

}