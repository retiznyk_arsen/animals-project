package org.retiznyk.animals.core.categories.descriptor;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;

import java.util.Comparator;

/**
 * Test {@link CarsCategoryDescriptor}.
 *
 * @author Arsen_Retiznyk
 **/
@RunWith(MockitoJUnitRunner.class)
public class CarsCategoryDescriptorTest {

    private static final String PROCESSED_ITEM = "item";
    private static final String PROCESSING_RESULT = "result";
    private static final int EXPECTED_ITEMS_SIZE = 1;

    @Mock
    private CategoryItemPreProcessor preProcessor;

    private CarsCategoryDescriptor target;

    @Before
    public void setUp() {
        target = spy(new CarsCategoryDescriptor(Mockito.mock(CategoryMatcher.class),
                StringUtils.EMPTY, preProcessor, Comparator.naturalOrder()));
    }

    @Test
    public void shouldInvokePreProcessorBeforeItemIsAddedCategory() {
        when(preProcessor.apply(PROCESSED_ITEM)).thenReturn(PROCESSING_RESULT);

        target.addItem(PROCESSED_ITEM);

        Assert.assertTrue(target.getCategoryModel().getItems().contains(PROCESSING_RESULT));
        Assert.assertEquals(EXPECTED_ITEMS_SIZE, target.getCategoryModel().getItems().size());
    }
}