package org.retiznyk.animals.core.utils;


import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Test {@link FileUtils}.
 *
 * @author Arsen_Retiznyk
 **/
public class FileUtilsTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowsIllegalArgumentExceptionWhenFileDoesNotExists() throws IOException, URISyntaxException, IllegalArgumentException {
        FileUtils.readFileFromClasspath("not-exists");
    }
}