package org.retiznyk.animals.core.categories.processor;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.retiznyk.animals.core.categories.processor.CategoryItemPreProcessorsChainBuilder.createChainBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.retiznyk.animals.api.categories.items.processor.CategoryItemPreProcessor;

/**
 * Test {@link CategoryItemPreProcessorsChainBuilder}.
 *
 * @author Arsen_Retiznyk
 **/
@RunWith(MockitoJUnitRunner.class)
public class CategoryItemPreProcessorsChainBuilderTest {

    private static final int FIRST_ARGUMENT = 0;
    private static final String FIRST_PREPROCESSOR_SUFFIX = "first";
    private static final String SECOND_PREPROCESSOR_SUFFIX = "second";
    private static final String TEST_VALUE = "test";

    @Mock
    private CategoryItemPreProcessor firstPreProcessor;

    @Mock
    private CategoryItemPreProcessor secondPreProcessor;

    @Before
    public void setUp() {
        when(firstPreProcessor.apply(anyString())).then(inv -> {
            String argument = inv.getArgument(FIRST_ARGUMENT);
            return argument + FIRST_PREPROCESSOR_SUFFIX;
        });

        when(secondPreProcessor.apply(anyString())).then(inv -> {
            String argument = inv.getArgument(FIRST_ARGUMENT);
            return argument + SECOND_PREPROCESSOR_SUFFIX;
        });
    }

    @Test
    public void shouldApplyProcessorsAddedToChain() {
        CategoryItemPreProcessor target = createChainBuilder().add(firstPreProcessor).build();

        String result = target.apply(TEST_VALUE);

        String expected = TEST_VALUE + FIRST_PREPROCESSOR_SUFFIX;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void shouldApplyAllPreProcessorsInAddingOrder() {
        CategoryItemPreProcessor target = createChainBuilder()
                .add(firstPreProcessor)
                .add(secondPreProcessor)
                .build();

        String result = target.apply(TEST_VALUE);

        String expected = TEST_VALUE + FIRST_PREPROCESSOR_SUFFIX + SECOND_PREPROCESSOR_SUFFIX;
        Assert.assertEquals(expected, result);
    }
}