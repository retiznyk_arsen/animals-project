package org.retiznyk.animals.core.categories.parser;

import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;
import org.retiznyk.animals.core.categories.descriptor.CategoryDescriptor;

import java.util.Collections;

/**
 * Test {@link CategoryItemParser}.
 *
 * @author Arsen_Retiznyk
 **/
@RunWith(MockitoJUnitRunner.class)
public class CategoryItemParserTest {

    private static final String FIRST_CATEGORY_NAME = "firstCategoryName";

    @Mock
    private CategoryMatcher firstMatcher;

    private CategoryItemParser target;

    @Before
    public void setUp() {
        CategoryDescriptor firstDescriptor = new CategoryDescriptor(firstMatcher, FIRST_CATEGORY_NAME);
        target = new CategoryItemParser(Collections.singletonList(firstDescriptor));

        when(firstMatcher.isCategory(FIRST_CATEGORY_NAME)).thenReturn(true);
    }

    @Test
    public void shouldChangeStateWhenAnotherCategoryWasDetected() {
        CategoryDescriptor currentDescriptor = target.getSubcategoryDefinition(StringUtils.EMPTY).getKey();

        boolean categoryItem = target.isCategoryItem(FIRST_CATEGORY_NAME);

        Assert.assertFalse(categoryItem);
        Assert.assertNotEquals(currentDescriptor, target.getSubcategoryDefinition(StringUtils.EMPTY).getKey());
    }
}