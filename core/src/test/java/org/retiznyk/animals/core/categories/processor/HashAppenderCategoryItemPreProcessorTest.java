package org.retiznyk.animals.core.categories.processor;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test {@link HashAppenderCategoryItemPreProcessor}.
 *
 * @author Arsen_Retiznyk
 **/
public class HashAppenderCategoryItemPreProcessorTest {

    private static final String TEST_VALUE = "TestValue";
    private static final String EXPECTED_VALUE = "TestValue (" + DigestUtils.md5Hex(TEST_VALUE) + ")";

    private HashAppenderCategoryItemPreProcessor target = new HashAppenderCategoryItemPreProcessor();

    @Test
    public void shouldTransformPassedParameterToLowercase() {
        Assert.assertEquals(EXPECTED_VALUE, target.apply(TEST_VALUE));
    }
}