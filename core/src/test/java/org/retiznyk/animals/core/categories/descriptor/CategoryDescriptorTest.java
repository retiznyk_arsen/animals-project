package org.retiznyk.animals.core.categories.descriptor;

import static org.mockito.Mockito.spy;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.retiznyk.animals.api.categories.matcher.CategoryMatcher;

/**
 * Test {@link CategoryDescriptor}.
 *
 * @author Arsen_Retiznyk
 **/
public class CategoryDescriptorTest {

    private static final String PROCESSED_ITEM = "item";
    private static final int EXPECTED_ITEMS_SIZE = 1;

    private CategoryDescriptor target;

    @Before
    public void setUp() {
        target = spy(new CategoryDescriptor(Mockito.mock(CategoryMatcher.class), StringUtils.EMPTY));
    }

    @Test
    public void shouldAddItemToCategory() {

        target.addItem(PROCESSED_ITEM);

        Assert.assertTrue(target.getCategoryModel().getItems().contains(PROCESSED_ITEM));
        Assert.assertEquals(EXPECTED_ITEMS_SIZE, target.getCategoryModel().getItems().size());
    }

}