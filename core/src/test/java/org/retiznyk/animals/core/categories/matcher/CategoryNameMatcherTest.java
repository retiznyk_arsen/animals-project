package org.retiznyk.animals.core.categories.matcher;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test {@link CategoryNameMatcher}.
 *
 * @author Arsen_Retiznyk
 **/
public class CategoryNameMatcherTest {

    private static final String CATEGORY_NAME = "name";
    private static final String NOT_CATEGORY_NAME = "not category name";

    private CategoryNameMatcher target = new CategoryNameMatcher(CATEGORY_NAME);

    @Test
    public void shouldReturnTrueIfPassedParameterEqualsCategoryName() {
        Assert.assertTrue(target.isCategory(CATEGORY_NAME));
    }

    @Test
    public void shouldReturnFalseIfPassedParameterNotEqualsCategoryName() {
        Assert.assertFalse(target.isCategory(NOT_CATEGORY_NAME));
    }

}