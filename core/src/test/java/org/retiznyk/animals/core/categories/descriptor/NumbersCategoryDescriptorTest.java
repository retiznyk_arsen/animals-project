package org.retiznyk.animals.core.categories.descriptor;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.retiznyk.animals.core.categories.model.NumbersCategoryModel;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Test {@link NumbersCategoryDescriptor}.
 *
 * @author Arsen_Retiznyk
 **/
@RunWith(Parameterized.class)
public class NumbersCategoryDescriptorTest {

    private static final String ONE = "one";
    private static final String TWO = "tru";
    private static final String THREE = "three";
    private static final String FOUR = "four";

    private final Collection<String> inputData;
    private final Collection<Map.Entry<String, Integer>> results;
    private final NumbersCategoryDescriptor target;


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Arrays.asList(ONE, ONE, ONE, ONE, ONE, ONE), Collections.singleton(new AbstractMap.SimpleEntry<>(ONE, 6))},
                {Arrays.asList(ONE, ONE, TWO, ONE, ONE, THREE, THREE, THREE),
                        Arrays.asList(new AbstractMap.SimpleEntry<>(ONE, 4),
                                new AbstractMap.SimpleEntry<>(TWO, 1),
                                new AbstractMap.SimpleEntry<>(THREE, 3))},
                {Arrays.asList(ONE, ONE, ONE, ONE, ONE, THREE), Arrays.asList(new AbstractMap.SimpleEntry<>(ONE, 5),
                        new AbstractMap.SimpleEntry<>(THREE, 1))},
                {Arrays.asList(FOUR, ONE, TWO, ONE, TWO, ONE, TWO, FOUR, FOUR), Arrays.asList(new AbstractMap.SimpleEntry<>(ONE, 3),
                        new AbstractMap.SimpleEntry<>(TWO, 3),
                        new AbstractMap.SimpleEntry<>(FOUR, 3))},
        });
    }

    public NumbersCategoryDescriptorTest(Collection<String> inputData, Collection<Map.Entry<String, Integer>> results) {
        this.inputData = inputData;
        this.results = results;
        target = new NumbersCategoryDescriptor(item -> true, StringUtils.EMPTY);
    }

    @Test
    public void shouldCountTheNumberOfTheSameItems() {
        inputData.forEach(target::addItem);

        boolean result = results.stream()
                .allMatch(e -> ((NumbersCategoryModel) target.getCategoryModel())
                        .getMapItems()
                        .get(e.getKey()).equals(e.getValue()));
        Assert.assertTrue(result);
    }
}