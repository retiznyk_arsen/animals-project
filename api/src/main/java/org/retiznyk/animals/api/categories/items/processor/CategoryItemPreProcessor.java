package org.retiznyk.animals.api.categories.items.processor;


import java.util.function.Function;

/**
 * The interface provides functionality for additional processing each item of a category.
 *
 * @author Arsen_Retiznyk
 */
public interface CategoryItemPreProcessor extends Function<String, String> {

}
