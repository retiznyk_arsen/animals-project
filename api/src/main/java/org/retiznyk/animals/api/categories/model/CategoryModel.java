package org.retiznyk.animals.api.categories.model;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.TreeSet;

/**
 * The class represents a category entity.
 *
 * @author Arsen_Retiznyk
 */
public class CategoryModel {
    private final String name;
    private final Collection<String> items;

    /**
     * Creates new category object.
     *
     * @param name - name of a category
     */
    public CategoryModel(@NotNull final String name) {
        this.name = name;
        this.items = new TreeSet<>();
    }

    /**
     * Creates new category object.
     *
     * @param name - name of a category.
     * @param items - items of a category.
     */
    public CategoryModel(@NotNull final String name,
                         @NotNull final Collection<String> items) {
        this.name = name;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public Collection<String> getItems() {
        return items;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name);
        items.forEach(i -> sb.append(System.lineSeparator()).append(i));
        return sb.toString();
    }
}
