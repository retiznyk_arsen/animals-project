package org.retiznyk.animals.api.categories.matcher;

/**
 * Matcher to identify categories.
 *
 * @author Arsen_Retiznyk
 */
public interface CategoryMatcher {

    /**
     * Checks if input data identifying a category.
     *
     * @param line - a data that can be used to identify kind of a category.
     * @return true if input data was defined as a category, if not returns false
     */
    boolean isCategory(final String line);
}
