### How to build:
1. Clone the repository `git clone https://gitlab.com/retiznyk_arsen/animals-project.git`
2. Go to the project folder `cd animals-project`
3. Build the project `mvn clean package`

### How to run:
1. To run use `java -jar core/target/core-1.0-SNAPSHOT.jar`
2. You can use additional parameters to choose from which resource the application will be read input data. To use this feature use `java -jar core/target/core-1.0-SNAPSHOT.jar [resource-name]` where `resource-name` can be **input.txt** or **input2.txt**(by default).
